import 'package:flutter/material.dart';
import 'split_view.dart';
import 'dart:html';
import 'dart:ui' as ui;
import 'dart:js' as js;
import 'package:universal_html/html.dart' as html;
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, this.title}) : super(key: key);

  final String? title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  late html.IFrameElement _elementpaypal;
  late html.IFrameElement _elementstripe;
  late html.IFrameElement _elementGooglepay;
  late html.IFrameElement _elementQias;
  @override
  void initState() {
    _elementpaypal = html.IFrameElement()
      ..width = "200px"
      ..height = "200px"
      ..style.border = 'none'
      ..srcdoc = """
        <!DOCTYPE html>
        <html>
          <body>
            <script src="https://www.paypal.com/sdk/js?client-id=sb"></script>
            <script>
              paypal.Buttons(
                {
                  createOrder: function(data, actions) {
                    return actions.order.create({
                      purchase_units: parent.purchase_units
                    });
                  },
                  onApprove: function(data, actions) {
                    return actions.order.capture().then(function(details) {
                      parent.flutter_feedback('Transaction completed by ' + details.payer.name.given_name);
                    });
                  }
                }

              ).render('body');
            </script>
          </body>
        </html>
        """;

    js.context["purchase_units"] = js.JsObject.jsify([
      {
        'amount': {'value': '0.02'}
      }
    ]);
    js.context["flutter_feedback"] = (msg) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(msg)));
    };

    // ignore:undefined_prefixed_name
    ui.platformViewRegistry.registerViewFactory(
      'PayPalButtons',
          (int viewId) => _elementpaypal,
    );



      _elementstripe= html.IFrameElement()
        ..width = "200px"
        ..height = "200px"
        ..style.border = 'none'
        ..src = "https://www.waze.com/fr/live-map";

      js.context["purchase_units"] = js.JsObject.jsify([
        {
          'amount': {'value': '0.02'}
        }
      ]);
      js.context["flutter_feedback"] = (msg) {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(msg)));
      };

      // ignore:undefined_prefixed_name
      ui.platformViewRegistry.registerViewFactory(
        'PayPalButtons',
            (int viewId) => _elementpaypal,
      );
      _elementstripe = html.IFrameElement()
        ..width = "200px"
        ..height = "200px"
        ..style.border = 'none'
        ..srcdoc = """
        <!DOCTYPE html>
        <html>
          <body>
  <h1>Purchase your new kit</h1>
  <!-- Paste your embed code script here. -->
  <script
    async
    src="https://js.stripe.com/v3/buy-button.js">
  </script>
  <stripe-buy-button
    buy-button-id='{{BUY_BUTTON_ID}}'
    publishable-key="pk_test_oKhSR5nslBRnBZpjO6KuzZeX"
  >
  </stripe-buy-button>
</body>
        </html>
        """;






    _elementGooglepay = html.IFrameElement()
      ..width = "200px"
      ..height = "200px"
      ..style.border = 'none'
      ..srcdoc = """
        <!DOCTYPE html>
        <html>
          <body>
 <div id="container"></div>
  <!-- Paste your embed code script here. -->
  <script  src="http://maps.googleapis.com/maps/api/js?sensor=false">
  const container = document.getElementById('container');
const button = googlePayClient.createButton({
  buttonColor: 'default',
  buttonType: 'buy',
  buttonRadius: 4,
  onClick: () => {},
  allowedPaymentMethods: [] // use the same payment methods as for the loadPaymentData() API call
});

container.appendChild(button);
alert("done")
  </script>

</body>
        </html>
        """;
    // ignore:undefined_prefixed_name
    ui.platformViewRegistry.registerViewFactory(
      'GooglepaypalButtons',
          (int viewId) => _elementGooglepay,
    );
    _elementQias = html.IFrameElement()
    ..width = "200px"
    ..height = "200px"
    ..style.border = 'none'
    ..srcdoc= """
        <!DOCTYPE html>
        <html>
          <body>

 
  
</body>
        </html>
        """;
    // ignore:undefined_prefixed_name
    ui.platformViewRegistry.registerViewFactory(
    'Qias',
    (int viewId) => _elementQias,
    );
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title!),
      ),
      body: SplitView(
        children: [
          SplitView(
            viewMode: SplitViewMode.Horizontal,
            indicator: SplitIndicator(viewMode: SplitViewMode.Horizontal),
            activeIndicator: SplitIndicator(
              viewMode: SplitViewMode.Horizontal,
              isActive: true,
            ),
            children: [
              Container(
                child: Center(child: HtmlElementView(viewType: 'PayPalButtons')),
                color: Colors.red,
              ),
              Container(
                child: Center(child: HtmlElementView(viewType: 'StripeButtons')),
                color: Colors.blue,
              ),
              Container(
                child: Center(child: HtmlElementView(viewType: 'GooglepaypalButtons')),
                color: Colors.green,
              ),
            ],
            onWeightChanged: (w) => print("Horizon: $w"),
          ),
          Container(
            child: Center(child: HtmlElementView(viewType: 'Qias')),
            color: Colors.purple,
          ),
          Container(
            child: Center(child: Text("View5")),
            color: Colors.yellow,
          ),
        ],
        viewMode: SplitViewMode.Vertical,
        indicator: SplitIndicator(viewMode: SplitViewMode.Vertical),
        activeIndicator: SplitIndicator(
          viewMode: SplitViewMode.Vertical,
          isActive: true,
        ),
        controller: SplitViewController(limits: [null, WeightLimit(max: 0.5)]),
        onWeightChanged: (w) => print("Vertical $w"),
      ),
    );
  }
}
